import axios from 'axios';
import * as api from './integrations/test';
import { describe, test, expect } from '@jest/globals';



describe("ToDo list mission ", () => {
  let listId: string;
  const Name: string = 'ljhfkhrd';
  let mission: any;

  test('Create a new list', async () => {
    const response = await api.CreatList(Name);
    listId = response.data.id;
    expect(response.status).toEqual(200);
    expect(response.data.name).toEqual(Name);
  })

  test('Create a new mission', async () => {
    const { data, status } = await api.CreatMission(listId, 'TaskId', new Date());
    expect(status).toEqual(200);
    expect(data.listId).toEqual(listId);
    mission = data;
  })

  test('Change value is complicted', async () => {
    //todo co mi1= getbid9(mid)
    const result = await api.UpComplicted(mission.id);
    expect(result.status).toEqual(200);
    //todo co mi1= getbid9(mid)
//expect
  })


  test('up mission by id', async () => {
    ////////////////
    const response = await api.UpMissionById(mission.id, {"listId": listId, "task": "changed!"})
    expect(response.status).toBeLessThan(300);
    /////////////////////
    ////////////
  })


  test('Change date', async () => {
    const time = new Date();
    const request = (await api.UpDate(mission.id, time));
    const theTime = (await api.GetMissionById(mission.id)).finishDate;
    expect(request.status).toBe(200);
    expect(time.toISOString()).toEqual(theTime);

  })

  test('Delete date', async () => {
    const before = (await api.GetMissionById(mission.id));
    expect(before).toHaveProperty("finishDate");
    const dalete = (await api.DeletDate(mission.id)).status;
    const after = (await api.GetMissionById(mission.id));
    expect(dalete).toBeLessThan(300);
    expect(after).not.toHaveProperty("finishDate");

  })


  test('Delete Mission by id', async () => {
    const before = (await api.GetMissionById(mission.id));
    const status = (await api.DeleteMissionById(mission.id)).status;
    const after = (await api.GetMissionById(mission.id));
    expect(status).toBeLessThan(300);
    expect(after).toEqual('');
  })

  test('Delete list by id', async () => {
    const before = (await api.GetListById(listId)).data;
    const a =await api.DeleteListById(listId)
    expect(a.status).toEqual(200);
    const after = (await api.GetListById(listId)).data;
    expect(after).toEqual('');

  })
})
// describe("ToDo list mission ", () => {

//   const Name: string = 'ljhfkhrd';


//   test('Delete list and mission by id', async () => {
//     const firstList = await api.CreatList(Name);
//     const allTasksArray = await api.GetAllMission();
//     const tasksOfaList = allTasksArray.filter((task: any) => {
//       return task.listId == firstList;
//     });
//     expect(allTasksArray).toEqual(expect.arrayContaining(tasksOfaList));

//     await api.DeleteListById(Name);
//     const newTasksArray = await api.GetAllMission();
//     const newListsArray = await api.GetAllList();
//     expect(newTasksArray.includes(tasksOfaList)).toEqual(false);
//     expect(newListsArray.includes(firstList)).toEqual(false);
//   });
// })       










// const = async () => {
//     const listName: string = 'ljhfkhrd';
//     const { listId } = await api.CreatList("guri");

//     const id = uuidv4();
//     const res = await api.CreatMission('17490e47-0558-4aae-ba9d-a1d518c9b56d', id, new Date());
//     console.log("🚀 ~ file: index.ts ~ line 11 ~ test ~ res", res.data)
// }

// t();