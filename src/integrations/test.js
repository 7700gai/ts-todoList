"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.DeleteListById = exports.CreatList = exports.GetListById = exports.GetAllList = exports.UpComplicted = exports.DeletDate = exports.UpDate = exports.UpMissionById = exports.DeleteMissionById = exports.CreatMission = exports.GetMissionById = exports.GetAllMission = void 0;
var axios_1 = require("axios");
// Get all mission
var GetAllMission = function () { return __awaiter(void 0, void 0, void 0, function () {
    var data;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"].get('http://localhost:4000/todo')];
            case 1: return [4 /*yield*/, (_a.sent()).data];
            case 2:
                data = (_a.sent()).data;
                return [2 /*return*/, data];
        }
    });
}); };
exports.GetAllMission = GetAllMission;
// Get mission by id
var GetMissionById = function (taskId) { return __awaiter(void 0, void 0, void 0, function () {
    var data;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"].get("http://localhost:4000/todo/" + taskId)];
            case 1: return [4 /*yield*/, (_a.sent())];
            case 2:
                data = (_a.sent()).data;
                return [2 /*return*/, data];
        }
    });
}); };
exports.GetMissionById = GetMissionById;
// create mission
var CreatMission = function (listId, task, finishDate) { return __awaiter(void 0, void 0, void 0, function () {
    var parameters;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                parameters = { "listId": listId, "task": task, "finishDate": finishDate };
                return [4 /*yield*/, axios_1["default"].post('http://localhost:4000/todo', parameters)];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); };
exports.CreatMission = CreatMission;
// Delete mission by id
var DeleteMissionById = function (taskId) { return __awaiter(void 0, void 0, void 0, function () {
    var abc;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"]["delete"]("http://localhost:4000/todo/" + taskId)];
            case 1:
                abc = _a.sent();
                return [2 /*return*/, abc];
        }
    });
}); };
exports.DeleteMissionById = DeleteMissionById;
// Updat value
var UpMissionById = function (taskId, parameters) { return __awaiter(void 0, void 0, void 0, function () {
    var UpMission;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"].put("http://localhost:4000/todo/" + taskId, parameters)];
            case 1:
                UpMission = _a.sent();
                return [2 /*return*/, UpMission];
        }
    });
}); };
exports.UpMissionById = UpMissionById;
// Change date
var UpDate = function (taskId, finishDate) { return __awaiter(void 0, void 0, void 0, function () {
    var abc;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"].put("http://localhost:4000/todo/date/" + taskId, { "finishDate": finishDate })];
            case 1:
                abc = _a.sent();
                return [2 /*return*/, abc];
        }
    });
}); };
exports.UpDate = UpDate;
// Delete date
var DeletDate = function (taskId) { return __awaiter(void 0, void 0, void 0, function () {
    var abc;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"]["delete"]("http://localhost:4000/todo/date/" + taskId)];
            case 1:
                abc = _a.sent();
                return [2 /*return*/, abc];
        }
    });
}); };
exports.DeletDate = DeletDate;
// Change value
var UpComplicted = function (taskId) { return __awaiter(void 0, void 0, void 0, function () {
    var abc;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log('i stop here');
                return [4 /*yield*/, axios_1["default"].put("http://localhost:4000/todo/toggle/" + taskId)];
            case 1:
                abc = _a.sent();
                return [2 /*return*/, abc];
        }
    });
}); };
exports.UpComplicted = UpComplicted;
// Get all list
var GetAllList = function () { return __awaiter(void 0, void 0, void 0, function () {
    var abc;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"].get('http://localhost:4000/todo-list')];
            case 1:
                abc = (_a.sent()).data;
                return [2 /*return*/, abc];
        }
    });
}); };
exports.GetAllList = GetAllList;
// Get list by id
var GetListById = function (Listid) { return __awaiter(void 0, void 0, void 0, function () {
    var abc;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"].get("http://localhost:4000/todo-list/" + Listid)];
            case 1:
                abc = _a.sent();
                return [2 /*return*/, abc];
        }
    });
}); };
exports.GetListById = GetListById;
// Create list
var CreatList = function (ListName) { return __awaiter(void 0, void 0, void 0, function () {
    var response;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"].post('http://localhost:4000/todo-list/', { "name": ListName })];
            case 1:
                response = _a.sent();
                return [2 /*return*/, response];
        }
    });
}); };
exports.CreatList = CreatList;
// Delete list by id
var DeleteListById = function (listId) { return __awaiter(void 0, void 0, void 0, function () {
    var abc;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios_1["default"]["delete"]("http://localhost:4000/todo-list/" + listId)];
            case 1:
                abc = _a.sent();
                return [2 /*return*/, abc];
        }
    });
}); };
exports.DeleteListById = DeleteListById;
