import axios from 'axios'
import { string } from 'yargs';


// Get all mission
export const GetAllMission = async ()=>{
    const {data} = await (await axios.get('http://localhost:4000/todo')).data;
    return data;
    
}

// Get mission by id
export const GetMissionById = async (taskId: string)=>{
    const {data} = await (await axios.get(`http://localhost:4000/todo/${taskId}`));
    return data;
    
}

// create mission
export const CreatMission = async (listId : string, task : string, finishDate?: Date) =>{
    const parameters = {"listId": listId, "task": task, "finishDate": finishDate };

    return await axios.post('http://localhost:4000/todo', parameters);    
}


// Delete mission by id
export const DeleteMissionById = async (taskId: string)=>{
    const abc = await axios.delete(`http://localhost:4000/todo/${taskId}`);
    return abc;
    
}

// Updat value
export const UpMissionById = async (taskId: string, parameters: object)=>{
    const UpMission = await axios.put(`http://localhost:4000/todo/${taskId}`, parameters);
    return UpMission;
    
}

// Change date
export const UpDate = async (taskId: string, finishDate: Date)=>{
    const abc = await axios.put(`http://localhost:4000/todo/date/${taskId}`, { "finishDate": finishDate });
    return abc;
    
}

// Delete date
export const DeletDate = async (taskId: string)=>{
    const abc = await axios.delete(`http://localhost:4000/todo/date/${taskId}`);
    return abc;
    
}

// Change value
export const UpComplicted = async (taskId: string)=>{
    console.log('i stop here');
    const abc = await axios.put(`http://localhost:4000/todo/toggle/${taskId}`);
    return abc;
    
}

// Get all list
export const GetAllList = async ()=>{
    const abc = (await axios.get('http://localhost:4000/todo-list')).data;
    return abc;
 
}

// Get list by id
export const GetListById = async (Listid: string)=>{
    const abc = await axios.get(`http://localhost:4000/todo-list/${Listid}`);
    return abc;
    
}

// Create list
export const CreatList = async (ListName: string,): Promise <any> => {
    const response = await axios.post('http://localhost:4000/todo-list/', { "name" :ListName });
    return response;
}

// Delete list by id
export const DeleteListById = async (listId: string)=>{
    const abc = await axios.delete(`http://localhost:4000/todo-list/${listId}`);
    return abc;
    
}
