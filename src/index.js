"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var api = require("./integrations/test");
var globals_1 = require("@jest/globals");
globals_1.describe("ToDo list mission ", function () {
    var listId;
    var Name = 'ljhfkhrd';
    var mission;
    globals_1.test('Create a new list', function () { return __awaiter(void 0, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, api.CreatList(Name)];
                case 1:
                    response = _a.sent();
                    listId = response.data.id;
                    globals_1.expect(response.status).toEqual(200);
                    globals_1.expect(response.data.name).toEqual(Name);
                    return [2 /*return*/];
            }
        });
    }); });
    globals_1.test('Create a new mission', function () { return __awaiter(void 0, void 0, void 0, function () {
        var _a, data, status;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, api.CreatMission(listId, 'TaskId', new Date())];
                case 1:
                    _a = _b.sent(), data = _a.data, status = _a.status;
                    globals_1.expect(status).toEqual(200);
                    globals_1.expect(data.listId).toEqual(listId);
                    mission = data;
                    return [2 /*return*/];
            }
        });
    }); });
    globals_1.test('Change value is complicted', function () { return __awaiter(void 0, void 0, void 0, function () {
        var result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, api.UpComplicted(mission.id)];
                case 1:
                    result = _a.sent();
                    globals_1.expect(result.status).toEqual(200);
                    return [2 /*return*/];
            }
        });
    }); });
    globals_1.test('up mission by id', function () { return __awaiter(void 0, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, api.UpMissionById(mission.id, { "listId": listId, "task": "changed!" })];
                case 1:
                    response = _a.sent();
                    globals_1.expect(response.status).toBeLessThan(300);
                    return [2 /*return*/];
            }
        });
    }); });
    globals_1.test('Change date', function () { return __awaiter(void 0, void 0, void 0, function () {
        var time, request, theTime;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    time = new Date();
                    return [4 /*yield*/, api.UpDate(mission.id, time)];
                case 1:
                    request = (_a.sent());
                    return [4 /*yield*/, api.GetMissionById(mission.id)];
                case 2:
                    theTime = (_a.sent()).finishDate;
                    globals_1.expect(request.status).toBe(200);
                    globals_1.expect(time.toISOString()).toEqual(theTime);
                    return [2 /*return*/];
            }
        });
    }); });
    globals_1.test('Delete date', function () { return __awaiter(void 0, void 0, void 0, function () {
        var before, dalete, after;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, api.GetMissionById(mission.id)];
                case 1:
                    before = (_a.sent());
                    globals_1.expect(before).toHaveProperty("finishDate");
                    return [4 /*yield*/, api.DeletDate(mission.id)];
                case 2:
                    dalete = (_a.sent()).status;
                    return [4 /*yield*/, api.GetMissionById(mission.id)];
                case 3:
                    after = (_a.sent());
                    globals_1.expect(dalete).toBeLessThan(300);
                    globals_1.expect(after).not.toHaveProperty("finishDate");
                    return [2 /*return*/];
            }
        });
    }); });
    globals_1.test('Delete Mission by id', function () { return __awaiter(void 0, void 0, void 0, function () {
        var before, status, after;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, api.GetMissionById(mission.id)];
                case 1:
                    before = (_a.sent());
                    return [4 /*yield*/, api.DeleteMissionById(mission.id)];
                case 2:
                    status = (_a.sent()).status;
                    return [4 /*yield*/, api.GetMissionById(mission.id)];
                case 3:
                    after = (_a.sent());
                    globals_1.expect(status).toBeLessThan(300);
                    globals_1.expect(after).toEqual('');
                    return [2 /*return*/];
            }
        });
    }); });
    globals_1.test('Delete list by id', function () { return __awaiter(void 0, void 0, void 0, function () {
        var before, a, after;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, api.GetListById(listId)];
                case 1:
                    before = (_a.sent()).data;
                    return [4 /*yield*/, api.DeleteListById(listId)];
                case 2:
                    a = _a.sent();
                    globals_1.expect(a.status).toEqual(200);
                    return [4 /*yield*/, api.GetListById(listId)];
                case 3:
                    after = (_a.sent()).data;
                    globals_1.expect(after).toEqual('');
                    return [2 /*return*/];
            }
        });
    }); });
});
globals_1.describe("ToDo list mission ", function () {
    var Name = 'ljhfkhrd';
    globals_1.test('Delete list end mission by id', function () { return __awaiter(void 0, void 0, void 0, function () {
        var firstList, allTasksArray, tasksOfaList, newTasksArray, newListsArray;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, api.GetListById("Name")];
                case 1:
                    firstList = _a.sent();
                    return [4 /*yield*/, api.GetAllMission()];
                case 2:
                    allTasksArray = _a.sent();
                    tasksOfaList = allTasksArray.filter(function (task) {
                        return task.listId == firstList;
                    });
                    globals_1.expect(allTasksArray).toEqual(globals_1.expect.arrayContaining(tasksOfaList));
                    return [4 /*yield*/, api.DeleteListById(Name)];
                case 3:
                    _a.sent();
                    return [4 /*yield*/, api.GetAllMission()];
                case 4:
                    newTasksArray = _a.sent();
                    return [4 /*yield*/, api.GetAllList()];
                case 5:
                    newListsArray = _a.sent();
                    globals_1.expect(newTasksArray.includes(tasksOfaList)).toEqual(false);
                    globals_1.expect(newListsArray.includes(firstList)).toEqual(false);
                    return [2 /*return*/];
            }
        });
    }); });
});
// const = async () => {
//     const listName: string = 'ljhfkhrd';
//     const { listId } = await api.CreatList("guri");
//     const id = uuidv4();
//     const res = await api.CreatMission('17490e47-0558-4aae-ba9d-a1d518c9b56d', id, new Date());
//     console.log("🚀 ~ file: index.ts ~ line 11 ~ test ~ res", res.data)
// }
// t();
