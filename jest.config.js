module.exports = {
    "roots": [
      "<rootDir>/src"
    ],
    "testMatch": [
      "**/index.ts"
    ],
    "transform": {
      "^.+\\.(ts|tsx)$": "ts-jest"
    },
  }